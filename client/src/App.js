import React from 'react';
//import './App.css';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-default/dist/all.css';

class App extends React.Component {

  constructor(props){
    super(props);
    this.updateInterval = 0;
    this.rankChangeInterval = 0;
    this.state = {
      playerList : []
    }
  }
  componentDidMount(){
    //First get data from default uri
    //It's not required, but some situation 
    //In first connect to redis cloud, connection failed
    //Because my cloud access free paid or i cannot configure properly
    fetch('https://warm-savannah-32238.herokuapp.com/get-players?rank=120').then(response => {  
      if(response.status !== 200){
        console.log('Player data cannot be getted.');
        throw new Error('Unreached Data');
      }
      return response.json();
    }).then(data => {
      const result = [];
      data.forEach((player, index) => {
        let player1 = JSON.parse(player);
        player1['dailydiff'] = 0;
        result.push(player1)
      });
      this.setState({playerList:result});
    }).catch((err) => {
      console.log(err);
    });
    //Every 5 seconds get redis data and  update leaderboard
    //If you change rank parameter, you will see properly result
    this.updateInterval = setInterval(() => {
      fetch('https://warm-savannah-32238.herokuapp.com/get-redis-data?rank=134').then(response => {
        if(response.status !== 200){
          console.log('Player data cannot be getted.');
          throw new Error('Unreached Data');
        }
        return response.json();
      }).then(data => {
        const result = [];
        data.forEach((player, index) => {
          let player1 = JSON.parse(player);
          player1['dailydiff'] = 0;
          result.push(player1)
        });
        this.setState({playerList:result});
      }).catch((err) => {
        console.log(err);
      });
    }, 5000);
    this.rankChangeInterval = setInterval(() => {
      fetch('https://warm-savannah-32238.herokuapp.com/get-rank-change').then((response) => {
        if(response.status != 200){
          console.log('Rank change data cannot be getted.');
          throw new Error('Unreached Data');
        }
        return response.json();
      }).then((data) => {
        let updated = this.state.playerList;
        updated.forEach((player) => {
          player.dailydiff = data[player.username]['last'] - data[player.username]['first'];
          console.log(player.dailydiff);
        })
        this.setState({playerList:updated});
      }).catch((err) => {
        console.log(err);
      });
      clearInterval(this.rankChangeInterval);
    }, 60000);
  }

  componentWillUnmount(){
    clearInterval(this.updateInterval);
    clearInterval(this.rankChangeInterval);

  }
  cellWithForeGround = (props) => {
    const dailydiff = props.dataItem.dailydiff;

    if (dailydiff > 0) {
      return (
        <td
          style={{
            color: "green",
          }}
        >
          {dailydiff}
        </td>
      );
    }else if(dailydiff < 0){
      return (
        <td
          style={{
            color: "red",
          }}
        >
          {dailydiff}
        </td>
      );      
    }

    return (
      <td
        style={{
          color: "black",
        }}
      >
        {dailydiff}
      </td>
    );
  };

  render() {
    return (
      <div className="App">
        <Grid
          data={this.state.playerList}
        >
          <GridColumn field='rank' title='RANK'/>
          <GridColumn field='username' title='PLAYER NAME'/>
          <GridColumn field='country' title='COUNTRY'/>
          <GridColumn field='money' title='MONEY'/>
          <GridColumn cell={this.cellWithForeGround} field='dailydiff' title='DAILY DIFF'/>
        </Grid>
      </div>
    );
  }
}

export default App;
