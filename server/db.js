const { MongoClient } = require('mongodb');
const URI = "mongodb+srv://ilteris:Ilteris_3152@leaderboard.zwe1p.mongodb.net/LeaderBoard?retryWrites=true&w=majority";

module.exports = class DBConnection {
  constructor(uri = URI){
    this.uri = uri;
    this.client = new MongoClient(this.uri, { useNewUrlParser: true, useUnifiedTopology: true });
    this.db = null;
    this.collections = {};
    this.status = null;
    this.data = [];
  }
  async connect(){
    await this.client.connect().catch(err => {
      console.log(`In DB connection, ${err}`);
      this.status = 'reject';
      throw err;
    });
    if(!this.client){
      console.log(`Mongo Client Null`);
      this.status = 'reject';
      return;
    }
    try{
      this.db = this.client.db('LeaderBoard');
    }catch(err){
      this.status = 'db_fail';
      console.log(err);
      return;
    }
    this.status = 'OK';
  }
  async createCollection(name='users', options={}){
    if(this.status !== 'OK'){
      console.log(`Collection cannot be created`);
      return false;
    }
    const isIn = await this.checkCollection(name);
    if(isIn){
      console.log(`Collection is exist.`);
      return true;
    }
    await this.db.createCollection(name, options).catch(err => {
      console.log(`On collection creating ${err}`);
      throw err;
    });
    if(!this.client){
      console.log(`Mongo Client Null`);
      this.status = 'reject';
      return false;
    }
    try{
      this.collections[name] = this.db.collection(name);
    }catch(err){
      console.log(err);
      return false;
    }
    console.log(`Collection ${name} created succesfully.`);
    return true;
  }

  async checkCollection(name){
    const collections = await this.db.listCollections().toArray();
    return collections.map(c => c.name).includes(name);
  }
  async getDocuments(name='users'){
    const result = await this.db.collection(name).find({}).toArray().catch(err => {
      console.log(err);
      result = [];
    });
    return result;
  }
  async createDocument(name, obj){
    await this.db.collection(name).insertOne(JSON.parse(obj)).catch(err =>{
      console.log(err);
      return false;
    });
    return true;
  }
  async deleteAll(name){
    await this.db.collection(name).deleteMany({}).catch(err => {
      console.log(err);
      return false;
    });
    return true;
  }
}
