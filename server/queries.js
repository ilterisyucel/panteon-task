const Utils = require('./utils');
const utils = new Utils();
module.exports = {
  getPlayers:  async (rank, lb, db, rankStatus) => {

    let playerList = [];
    let responseSet = [];
    if(lb.isConnected){
      //If redis server is connected, datas are getted from redis
      //This control is made beacuse sometimes redis cloud server is
      //not stable to free paid plan.
      console.log('Connect Status redis server is OK');
      const redisRecord = await lb.getAllUser('plist');
      if(redisRecord.length > 0){
        redisRecord.forEach((player, index) => {
          const obj = JSON.parse(player);
          playerList.push(obj);
          rankStatus[obj.username] = {'first' : index, 'last' : index};
        });
      }else{
        //If redis server is connected, but not has records,
        //records getted from mongodb and add it. Redis server is used
        //by data cache.
        console.log('Connect Status redis server is OK but records is missing');
        const mongoRecord = await db.getDocuments('plist');
        mongoRecord.forEach((player) => {
          lb.addUser(player.money, JSON.stringify(player));
          playerList.push(player);
          rankStatus[player.username] = {'first' : index, 'last' : 0};
        })
        //If data cannot get redis sort set, sorted as descending order
        //based on money
        playerList.sort((obj, obj1) => obj1.money - obj.money);
      }
    }else{
      //If redis server is not connected, datas getted from mongodb.
      console.log('Could not connect Status redis server');
      const mongoRecord = await db.getDocuments('plist');
      mongoRecord.forEach((player) => {
        playerList.push(JSON.parse(player));
        rankStatus[player.username] = {'first' : index, 'last' : 0};
      })
      playerList.sort((obj, obj1) => obj1.money - obj.money);    
    }
    //To show leader board which requested format
    //Set is used.
    //If data list is less than equal 100, all items
    //are added. If not, slicing list.
    //After that list convert to set and added above 2,
    //below 3 items. For example requested rank is 101
    //we added 100th, 99th element yet. Because of use
    //set, these elements are not added.
    if(playerList.length > 100)
      //responseSet = new Set(playerList.slice(0, 100));
      responseSet = utils.handleArrRank(playerList.slice(0, 100));
    else
      responseSet = utils.handleArrRank(playerList.slice());
    try{
      utils.handleItemRank(responseSet, playerList[rank-3], rank-3);
      utils.handleItemRank(responseSet, playerList[rank-2], rank-2);
      utils.handleItemRank(responseSet, playerList[rank-1], rank-1);
      utils.handleItemRank(responseSet, playerList[rank], rank);
      utils.handleItemRank(responseSet, playerList[rank+1], rank+1);
      utils.handleItemRank(responseSet, playerList[rank+2], rank+2);
    }catch(error){
      console.error(error);
      return false;
    }
    return responseSet;
  
  },
  getRedisData : async (rank, lb, rankStatus) => {
    let responseSet = new Set();
    if(!lb.isConnected)
      return responseSet;
    const playerList = await lb.getAllUser();
    utils.handleRankStatus(playerList, rankStatus);
    if(playerList.length > 100)
      responseSet = utils.handleArrRank(playerList.slice(0, 100));
    else
      responseSet = utils.handleArrRank(playerList.slice());
    try{
      utils.handleItemRank(responseSet, playerList[rank-3], rank-3);
      utils.handleItemRank(responseSet, playerList[rank-2], rank-2);
      utils.handleItemRank(responseSet, playerList[rank-1], rank-1);
      utils.handleItemRank(responseSet, playerList[rank], rank);
      utils.handleItemRank(responseSet, playerList[rank+1], rank+1);
      utils.handleItemRank(responseSet, playerList[rank+2], rank+2);
    }catch(error){
      console.error(error);
      return false;
    }
    return responseSet;

  }
}