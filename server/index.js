const express = require('express');
const cors = require('cors');
const path = require('path');
const queries = require('./queries');
const UPDATE_TIME = 10000;
const DAY = 60000;
const WEEK = 600000;
let updateInterval = 0;
let dayInterval = 0;
let rankStatus = {};

const PORT = process.env.PORT || 3005;
const corsOptions = {
  origin: '*'
};
const Leaderboard = require('./LeaderBoard.js');
const User = require('./User.js');
const DBConnection = require('./db');
const Utils = require('./utils');
const Socket = require('./socket');
const { get } = require('http');

const app = express();
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const lb = new Leaderboard.LeaderBoard();
const db = new DBConnection();
const utils = new Utils();
//Socket is a better tool for instant data exchange. However, 
//I couldn't make it.
const socket = new Socket({
  port: PORT,
  path: '/socket'
});

app.get('/get-rank-change', (req, res) => {
  if(!Object.keys(rankStatus).length)
    return res.status(404).send("RANK CHANGE CANNOT PROCESS");
  return res.status(200).json(rankStatus);
})
app.get('/get-redis-data', async(req, res) => {
  const  rank  = parseInt(req.query.rank);
  if(!rank)
    return res.status(400).send('BAD REQUEST!'); 
  const responseSet = await queries.getRedisData(rank, lb, rankStatus);
  if(!responseSet)
    return res.status(204).send('NO CONTENT');

  return res.status(200).json([...responseSet]);
})
app.get('/get-players', async (req, res) => {
  const  rank  = parseInt(req.query.rank);
  if(!rank)
    return res.status(400).send('BAD REQUEST!');
  const responseSet = await queries.getPlayers(rank, lb, db, rankStatus);
  if(!responseSet)
    return res.status(204).send('NO CONTENT');

  return res.status(200).json([...responseSet]);
});

(async() => {
  await db.connect();
  if(db.status === 'OK'){
    app.listen(PORT, async() => {
      console.log(`Server is running on port ${PORT}...`);
      await db.createCollection('plist', {});
      const mongoRecord = await db.getDocuments('plist');
      if(mongoRecord.length <= 0){
        console.log("Mongo DB empty");
        await utils.documentGenerator(db, 'plist');
      }
      //start update redis interval.
      //so record on redis updated.
      //After that, in front end values updated.
      updateInterval = setInterval(async () => {
        const ret = await utils.updateRedisRecords(lb);
      }, UPDATE_TIME);
    });      
  }else{
    console.log(`Server cannot run because db connection fail`);
  }
  
})();

