const User = require('./User');
const { v4: uuidv4 } = require('uuid');
module.exports = class Utils {
  constructor(){

  }
  deepEqualCheck(object1, object2){
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);
    if (keys1.length !== keys2.length) {
      return false;
    }
    for (const key of keys1) {
      const val1 = object1[key];
      const val2 = object2[key];
      const areObjects = this.isObject(val1) && isObject(val2);
      if (
        areObjects && !deepEqual(val1, val2) ||
        !areObjects && val1 !== val2
      ) {
        return false;
      }
    }
    return true;
  }
  isObject(object){
    return object != null && typeof object === 'object';
  }

  async documentGenerator(db, collectionName) {
    for(let i = 0; i < 5000; i++){
      const username = uuidv4();
      const country = 'Turkey';
      const money = 0;
      let user = new User(country, username, money);
      const status = await db.createDocument(collectionName, JSON.stringify(user));
      const message = status ? 'Player created succesfully' : 'Cannot create player';
      console.log(message);
    }
  }

  randomUserName(length){
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(this.randomInt(0, charactersLength));
   }
   return result;
  }

  randomInt(min, max){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  async updateRedisRecords(lb){
    const redisRecord = await lb.getAllUser();
    //Will be add, handling get score return false case
    //Optimal case I must not keep all user data on redis sorted set.
    //I just keep score and unique field and I might get other user
    //data from mongodb.
    redisRecord.forEach(async (elem) => {
      const score = await lb.getScore(elem);
      if(score !== false){
        const factor = this.randomInt(-10, 10);
        const nElem = JSON.parse(elem);
        nElem['money'] = score + factor;
        await lb.removeOne(elem);
        await lb.addUser(score + factor, JSON.stringify(nElem));
      } 
    });
  }
  //To solve show properly leaderboard as rank.
  //To solving on set reference problem, object with rank values
  //parsing to string and parse on ui to json.
  handleArrRank(arr){
    let returnSet = new Set();
    arr.forEach((elem, index) => {
      if(typeof elem === 'string')
        elem = JSON.parse(elem);
      elem['rank'] = index+1;
      returnSet.add(JSON.stringify(elem));
    });
    return returnSet;
  }

  handleItemRank(set, elem, rank){
    if(typeof elem === 'string')
      elem = JSON.parse(elem);
    elem['rank'] = rank+1;
    set.add(JSON.stringify(elem));
  }

  handleRankStatus(arr, obj){
    if(!arr.length || !Object.keys(obj).length)
      return;
    arr.forEach((player, index) => {
      const o = JSON.parse(player);
      obj[o.username]['last'] = index;
    })
  }

}