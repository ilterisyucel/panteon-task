const Redis = require('ioredis');
const HOST = 'redis-15937.c9.us-east-1-4.ec2.cloud.redislabs.com';
const REDIS_PORT = 15937;
const PASSWORD = '2JjbFbynoichjfFf1JhC7BUNpz9GBOmx'

const SET_KEY = 'plist';

class LeaderBoard {
  constructor(host = HOST, port = REDIS_PORT, password = PASSWORD, setKey = SET_KEY){
    this.setKey = setKey;
    this.isConnected = false;
    this.client = new Redis({
      host: host,
      port: port,
      password: password,
      retryStrategy(times) {
        return 300000;
      },
    });
    this.client.on('connect', () => {
      console.log(`Redis client connect on host: ${this.client.options.host} port: ${this.client.options.port}`);
      this.isConnected = true;
    });
    this.client.on('reconnecting', (event) => {
      console.log(`${event} after reconnect on redis server : ${this.client.options.host}`);
    })
    this.client.on('error', (err) => {
      console.log(`${err} on redis client`);
      console.log(`Could not connect address : ${this.client.options.host}`);
      //this.client.quit();
      this.isConnected = false;
    });
    this.data = [];
  };
  async addUser(score, obj){
    const status = await this.client.zadd(this.setKey, score, obj);
    return status;
  }
  async setUser(score, user){
    const status = await this.client.set(user, score);
    return status;
  }
  async getAllUser(setKey=this.setKey){
    const result = await this.client.zrevrange(setKey, 0, -1);
    return result;
  }
  async getAllUserWithScores(setKey=this.setKey){
    const result = await this.client.zrevrange(setKey, 0, -1, 'WITHSCORES');
    return result;
  }
  async removeAll(){
    const result = await this.client.zrange(this.setKey, 0, -1);
    result.forEach(async (player) => {
      const result = await this.client.zrem(this.setKey, player);
    })
  }
  async removeOne(elem){
    const result = await this.client.zrem(this.setKey, elem);
  }
  async getScore(elem){
    const score = await this.client.zscore(this.setKey, elem);
    if(score !== null)
      return parseInt(score);
    return false;
  }
}

module.exports.LeaderBoard = LeaderBoard;