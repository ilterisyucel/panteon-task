const  ws  = require('ws');
const http = require('http');

module.exports = class Socket {
  constructor(params){
    this.params = params;
    this.httpServer = http.createServer();
    this.socket = new ws.Server({'server' : this.httpServer, 'path' : params.path});
    this.socket.on('listening', () => {
      console.log('Socket is listen now.');
    });
    this.socket.on('connection', (ws) => {
      console.log('Client Connected.')
      ws.on('close', ()  => console.log('Client Disconnected'));
      ws.on('message', async (_message) => this.messageHandler(_message));
      ws.on('error', (_error) => this.errorHandler(_error));
    });
    this.socket.on('error', (error) => {
      console.log(`${error} on web socket`);
    })
  }
  messageHandler(_message){
    console.log(message);
  }
  errorHandler(_error){
    console.log(_error);
  }
}