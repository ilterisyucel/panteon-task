# H1 PANTEON TASK

### H3 Shortcomings

1. No daily and weekly rank changes added
2. Color change in ui based on daily rank change is not added.
3. The relationship between Redis and mongodb could not be designed for instant data flow.
4. The full potential of Kendo UI was not used.

### H3 Doned

1. It was used as a redis cache mechanism and as a mongodb db mechanism.
2. Used from redis and mongodb cloud service. Published with backend heroku.
3. Kendo UI is used.
4. Instant data  change was partially achieved.


## Running

Backend run on heroku. Mongodb db run on mongodb atlas and redis run on redis cloud. <br>

### Start

```

cd <path-to-client> client
npm start

```
